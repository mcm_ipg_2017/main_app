package toymobi.book1.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private static final int SPLASH_SCREEN_1_TIME = 2000;
    private static final int SPLASH_SCREEN_2_TIME = 4000;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        goSplash();
    }

    private void goSplash() {

        final View toymobi_blue_logo = findViewById(R.id.toymobi_blue_logo);

        final View splash_bkg = findViewById(R.id.splash_cover);

        if (toymobi_blue_logo != null && splash_bkg != null) {

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    toymobi_blue_logo.setVisibility(View.INVISIBLE);
                    splash_bkg.setVisibility(View.VISIBLE);
                }

            }, SPLASH_SCREEN_1_TIME);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    goMainScreen();
                }

            }, SPLASH_SCREEN_2_TIME);
        }
    }

    private void goMainScreen() {
        final Intent mainActivityIntent = new Intent(SplashScreen.this, MainActivity.class);
        SplashScreen.this.startActivity(mainActivityIntent);
        SplashScreen.this.finish();
    }


}