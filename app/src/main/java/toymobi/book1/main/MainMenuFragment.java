package toymobi.book1.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.book.BookFrag;
import com.toymobi.book_spc.BookSPCFrag;
import com.toymobi.booklgp.BookLGPFrag;
import com.toymobi.drawgame.GameDrawingFrag;
import com.toymobi.framework.raw.GetTextRawFile;
import com.toymobi.memorygame.MainFragmentMemory;
import com.toymobi.puzzlegame.GamePuzzleFrag;
import com.toymobi.recursos.BaseFragment;
import com.toymobi.recursos.EduardoStuff;
import com.toymobi.wordgame.WordGameFragment;

@SuppressWarnings("WeakerAccess")
public class MainMenuFragment extends BaseFragment {

    private static final int[] options = {R.id.book_option, R.id.book_spc_option,
            R.id.book_lgp_option, R.id.draw_option, R.id.memory_option, R.id.puzzle_option,
            R.id.word_option};

    static final String MAIN_MENU_FRAGMENT_TAG = "MAIN_MENU_FRAGMENT";

    private OnClickListener optionOnClickListener;

    private CharSequence pageInfoText = null;

    private Fragment bookFrag, bookSPCFrag, bookLGPFrag,
            gameDrawingFrag, wordGameFragment, memoryGameFrag, puzzleGameFrag;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        createMusicBackground(R.string.path_sound_game_menu, R.raw.minigame_sound, true);

        createSFX(com.toymobi.book.R.raw.sfx_normal_click);

        createVibrationeedback();
    }

    @Override
    public final View onCreateView(@NonNull final LayoutInflater inflater,
                                   final ViewGroup container,
                                   final Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.mainapp_fragment, container, false);

        createToolBar(R.id.toolbar_main, R.string.app_name_toolbar);

        createOptionClickListener(mainView);

        EduardoStuff.isMainMenu = true;

        return mainView;
    }

    private void createOptionClickListener(final View view) {
        if (optionOnClickListener == null) {
            optionOnClickListener = new OnClickListener() {

                @Override
                public void onClick(final View view) {
                    if (view != null) {
                        final int viewID = view.getId();
                        goOption(viewID);

                    }
                }
            };
        }

        for (int option : options) {

            final View optionView = view.findViewById(option);

            if (optionView != null && optionOnClickListener != null) {
                optionView.setOnClickListener(optionOnClickListener);
            }
        }
    }

    @Override
    public final void onCreateOptionsMenu(@NonNull final Menu menu,
                                          @NonNull final MenuInflater inflater) {

        menu.clear();

        inflater.inflate(R.menu.main_menu, menu);

        itemSound = menu.findItem(R.id.sound_main_menu);

        setIconSoundMenu();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        playFeedBackButtons();

        switch (item.getItemId()) {
            case R.id.info_main_menu:
                goToInfo();
                return true;

            case R.id.sound_main_menu:

                changeSoundMenu();

                return true;

            case R.id.exit_menu:
                EduardoStuff.showMessageExitSPC(getActivity());
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goToInfo() {

        if (pageInfoText == null) {
            pageInfoText = GetTextRawFile.getTextRawFile(getResources(), R.raw.info_app);
        }

        if (pageInfoText != null && pageInfoText.length() > 0) {
            EduardoStuff.showInfoNew(getActivity(), pageInfoText);
        }
    }

    @Override
    public final void onResume() {
        super.onResume();
        EduardoStuff.setLanguage(getActivity());
    }

    @Override
    public void deallocate() {

        super.deallocate();

        if (pageInfoText != null) {
            pageInfoText = null;
        }
    }

    private void goOption(final int viewID) {

        playSfxFeedBackButtons();

        final AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();

        if (appCompatActivity != null) {

            final FragmentManager fragmentManager = appCompatActivity.getSupportFragmentManager();

            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                    android.R.anim.fade_in, android.R.anim.fade_out);

            EduardoStuff.isMainMenu = false;


            switch (viewID) {
                case R.id.book_option:

                    if (bookFrag == null) {
                        bookFrag = new BookFrag();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            bookFrag,
                            BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                    break;

                case R.id.book_spc_option:

                    if (bookSPCFrag == null) {
                        bookSPCFrag = new BookSPCFrag();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            bookSPCFrag,
                            BookSPCFrag.FRAGMENT_TAG_BOOK_SPC_TEXT);

                    break;
                case R.id.book_lgp_option:


                    if (bookLGPFrag == null) {
                        bookLGPFrag = new BookLGPFrag();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            bookLGPFrag,
                            BookLGPFrag.FRAGMENT_TAG_BOOK_LGP);


                    break;
                case R.id.draw_option:

                    if (gameDrawingFrag == null) {
                        gameDrawingFrag = new GameDrawingFrag();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            gameDrawingFrag,
                            GameDrawingFrag.FRAGMENT_TAG_GAME_DRAWING);


                    break;
                case R.id.memory_option:

                    if (memoryGameFrag == null) {
                        memoryGameFrag = new MainFragmentMemory();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            memoryGameFrag,
                            MainFragmentMemory.FRAGMENT_TAG);

                    break;

                case R.id.puzzle_option:

                    if (puzzleGameFrag == null) {
                        puzzleGameFrag = new GamePuzzleFrag();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            puzzleGameFrag,
                            GamePuzzleFrag.FRAGMENT_TAG_GAME_PUZZLE);

                    break;
                case R.id.word_option:

                    if (wordGameFragment == null) {
                        wordGameFragment = new WordGameFragment();
                    }

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment,
                            wordGameFragment,
                            WordGameFragment.FRAGMENT_TAG_GAME_WORD);

                    break;
            }

            fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commitAllowingStateLoss();
        }
    }
}