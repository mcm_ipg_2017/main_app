package toymobi.book1.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.recursos.BaseFragment;
import com.toymobi.recursos.EduardoStuff;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout);

        if (savedInstanceState == null) {
            startFragment();
        }

    }

    private void startFragment() {

        final FragmentManager fragmentManager = getSupportFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.setCustomAnimations(
                android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);


        final BaseFragment mainMenuFragment = new MainMenuFragment();

        fragmentTransaction.replace(
                R.id.main_layout_container_fragment,
                mainMenuFragment,
                MainMenuFragment.MAIN_MENU_FRAGMENT_TAG);

//        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        // showMessageExit();
        if (EduardoStuff.isMainMenu) {
            EduardoStuff.showMessageExitSPC(this);
        } else {
            EduardoStuff.showMessageBackMainMenuSPC(this);
        }
    }
}
