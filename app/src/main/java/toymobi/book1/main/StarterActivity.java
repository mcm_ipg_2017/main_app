package toymobi.book1.main;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

public class StarterActivity extends AppCompatActivity {

    private static final String IS_INSTALL_TAG = "isAppInstalledEduardo";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkInstall();

        goMainScreen();

    }

    private void goMainScreen() {
        final Intent mainActivityIntent = new Intent(StarterActivity.this,
                SplashScreen.class);
        StarterActivity.this.startActivity(mainActivityIntent);
        StarterActivity.this.finish();
    }

    private void addShortcut() {

        final Context context = getApplicationContext();

        if (Build.VERSION.SDK_INT < 26) {
            //noinspection deprecation
            addShortCutOldApi(context);
        } else {
            addShortCutNewApi(context);
        }

    }

    private void addShortCutNewApi(Context context) {

        if (Build.VERSION.SDK_INT >= 26) {
            final ShortcutManager shortcutManager = getSystemService(ShortcutManager.class);

            if (shortcutManager.isRequestPinShortcutSupported()) {

                Intent intent = new Intent(context, getClass());

                intent.setAction(Intent.ACTION_MAIN);

                ShortcutInfo pinShortcutInfo = new ShortcutInfo
                        .Builder(context, "eduardo_profile")
                        .setIcon(Icon.createWithResource(context, R.mipmap.eduardo_icon))
                        .setIntent(intent)
                        .setShortLabel(getString(R.string.app_name))
                        .build();

                Intent pinnedShortcutCallbackIntent = shortcutManager.createShortcutResultIntent(pinShortcutInfo);

                PendingIntent successCallback = PendingIntent.getBroadcast(context,
                        0
                        , pinnedShortcutCallbackIntent,
                        0
                );

                shortcutManager.requestPinShortcut(pinShortcutInfo, successCallback.getIntentSender());
            }
        }
    }

    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    private void addShortCutOldApi(Context context) {
        final Intent shortcutIntent = new Intent(context, StarterActivity.class);

        shortcutIntent.setAction(Intent.ACTION_MAIN);

        final Intent addIntent = new Intent();

        final String name = getString(R.string.app_name);

        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);

        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(context, R.mipmap.eduardo_icon));

        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

        context.sendBroadcast(addIntent);
    }

    private void checkInstall() {

        boolean isAppInstalled;

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (sharedPreferences != null) {
            isAppInstalled = sharedPreferences.getBoolean(IS_INSTALL_TAG, false);

            if (!isAppInstalled) {
                addShortcut();

                final SharedPreferences.Editor editor = sharedPreferences.edit();

                if (editor != null) {
                    editor.putBoolean(IS_INSTALL_TAG, true);
                    editor.apply();
                }
            }
        }
    }
}